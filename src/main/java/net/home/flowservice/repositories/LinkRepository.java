package net.home.flowservice.repositories;

import net.home.flowservice.model.Link;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Repository
public class LinkRepository {

    private final List<Link> links;

    public LinkRepository() {

        links = Arrays.asList(
            new Link(1, 1, "jira"),
            new Link(2, 1, "tfs"),
            new Link(3, 1, "rally"),

            new Link(4, 2, "jira"),
            new Link(5, 2, "tfs"),

            new Link(6, 4, "rally"),

            new Link(7, 5, "jira"),
            new Link(8, 5, "rally")
        );
    }

    public List<Link> getByFlowId(int flowId) {

        return links
            .stream()
            .filter(link -> link.getFlowId() == flowId)
            .collect(toList());
    }
}
