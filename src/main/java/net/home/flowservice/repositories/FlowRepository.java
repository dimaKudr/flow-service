package net.home.flowservice.repositories;

import net.home.flowservice.model.Flow;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Repository
public class FlowRepository {

    private final List<Flow> flows;

    public FlowRepository() {

        flows = Arrays.asList(
            new Flow(1, "First Flow"),
            new Flow(2, "Main"),
            new Flow(3, "Test"),
            new Flow(4, "Login"),
            new Flow(5, "Sign up")
        );
    }

    public List<Flow> getFlows(int count, int offset) {

        return flows.stream().skip(offset).limit(count).collect(toList());
    }
}
