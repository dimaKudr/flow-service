package net.home.flowservice.repositories;

import net.home.flowservice.model.Sub;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class SubRepository {

    private final List<Sub> subs;

    public SubRepository() {

        subs = Arrays.asList(
            new Sub(1, 2, 1),
            new Sub(2, 3, 1),
            new Sub(3, 4, 1),
            new Sub(4, 2, 5),
            new Sub(5, 4, 5)
        );
    }
}
