package net.home.flowservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Sub {

    private int id;

    private int childId;
    private int parentId;
}
