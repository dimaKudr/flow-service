package net.home.flowservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Link {

    private int id;
    private int flowId;

    private String type;
}
