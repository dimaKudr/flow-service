package net.home.flowservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Flow {

    private int id;
    private String name;
}
