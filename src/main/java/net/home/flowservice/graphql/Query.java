package net.home.flowservice.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import net.home.flowservice.model.Flow;
import net.home.flowservice.repositories.FlowRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Query implements GraphQLQueryResolver {

    private final FlowRepository repository;

    public Query(FlowRepository repository) {
        this.repository = repository;
    }

    public List<Flow> getFlows(int count, int offset) {

        return repository.getFlows(count, offset);
    }
}
