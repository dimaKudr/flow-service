package net.home.flowservice.graphql;

import com.coxautodev.graphql.tools.GraphQLResolver;
import net.home.flowservice.model.Flow;
import net.home.flowservice.model.Link;
import net.home.flowservice.repositories.LinkRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FlowResolver implements GraphQLResolver<Flow> {

    private final LinkRepository linkRepository;

    public FlowResolver(LinkRepository linkRepository) {
        this.linkRepository = linkRepository;
    }

    public List<Link> getLinks(Flow flow) {

        return linkRepository.getByFlowId(flow.getId());
    }
}
